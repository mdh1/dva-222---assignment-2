﻿using System;
using System.Drawing;

namespace Assignment2
{
    public class BoostBox : Shape
    {
        System.Drawing.Rectangle rect = new Rectangle(0, 0, 0, 0);
        private Pen bluePen = new Pen(Color.Blue);


        public BoostBox(int x, int y, int sizeX, int sizeY) : base (new Point (x, y))
        {
            rect.X = x;
            rect.Y = y;
            rect.Width = sizeX;
            rect.Height = sizeY;
        }
    }
}
