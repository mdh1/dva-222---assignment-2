﻿using System;
using System.Drawing;

namespace Assignment2
{
	public class Ball : Shape
	{
        private Pen pen = new Pen(Color.Pink);
		private int radius;

        public Ball(float x, float y, int radius) : base(new PointF(x, y))
		{
			this.radius = radius;
		}

        public float GetX() { return this.position.X; }
        public float GetY() { return this.position.Y; }
        public int GetRadius() { return this.radius; }

		override public void Draw(Graphics g)
		{
			g.DrawEllipse(pen,position.X - radius, position.Y - radius, 2 * radius, 2 * radius);
		}

		public void Move()
		{
			position.X = position.X + speed.X;
			position.Y = position.Y + speed.Y;
		}

		private Vector speed;

		public Vector Speed
		{
			get { return speed; }
			set { speed = value; }
		}

	}
}
