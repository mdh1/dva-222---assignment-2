﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Assignment2
{
    public class TestDrawing
    {
        public void drawRect(Graphics g)
        {
            Brush blue = new SolidBrush(Color.Blue);
            Brush Green = new SolidBrush(Color.Green);
            Brush Red = new SolidBrush(Color.Red);
            Brush Beige = new SolidBrush(Color.Beige);
            Pen BluePen = new Pen(blue, 1);
            Pen GreenPen = new Pen(Green, 1);
            Pen GreenLine = new Pen(Green, 2);
            Pen RedPen = new Pen(Red, 1);
            Pen RedPen2 = new Pen(Red, 2);
            Pen BeigePen = new Pen(Beige, 1);

            g.DrawRectangle(BluePen, 70, 480, 295, 50);
            g.DrawRectangle(RedPen, 187, 170, 80, 80);
            g.DrawRectangle(BluePen, 524, 76, 79, 80);
            g.DrawRectangle(RedPen2, 650, 312, 55, 110);
            g.DrawLine(GreenPen, 202, 421, 607, 421);
            g.DrawLine(GreenLine, 506, 487, 733, 487); // 487 487
            g.DrawLine(GreenLine, 69, 44, 310, 44); // 44 44
            g.DrawLine(GreenLine, 136, 136, 365, 136);
            g.DrawLine(GreenLine, 408, 44, 686, 44);
        }
    }
}