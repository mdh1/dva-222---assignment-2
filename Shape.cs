﻿using System.Drawing;

namespace Assignment2
{
	public abstract class Shape
	{
		protected PointF position;

        public Shape(PointF position)
		{
			this.position = position;
		}

		public abstract void Draw(Graphics g);
	}
}
