﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Assignment2
{
    public class Engine
    {
        private MainForm form;
        private Timer timer;

        public enum LineAxis { Vertical, Horizontal };
        public enum RectangleState { SpeedUp, SpeedDown };

        private ISet<Ball> balls = new HashSet<Ball>();
        private ISet<Rectangle> rectangles = new HashSet<Rectangle>();
        private ISet<Line> lines = new HashSet<Line>();
        private Random random = new Random();

        public Engine()
        {
            form = new MainForm(); // Det står faktiskt Mainform form = new Mainform(); - Gömt info...
            timer = new Timer();
            AddBall();

            // Speed down rectangles
            AddRectangle(70, 480, 295, 50, RectangleState.SpeedDown);
            AddRectangle(524, 76, 79, 80, RectangleState.SpeedDown);

            // Speed up rectangles
            AddRectangle(187, 170, 80, 80, RectangleState.SpeedUp);
            AddRectangle(650, 312, 55, 110, RectangleState.SpeedUp);

            // Horizontal lines
            AddLine(202, 421, 607, 421, LineAxis.Horizontal);
            AddLine(506, 487, 733, 487, LineAxis.Horizontal);
            AddLine(69, 44, 310, 44, LineAxis.Horizontal);
            AddLine(136, 136, 365, 136, LineAxis.Horizontal);
            AddLine(408, 44, 686, 44, LineAxis.Horizontal);
            AddLine(30, 550, 680, 550, LineAxis.Horizontal);

            // Vertical lines
            AddLine(25, 100, 25, 550, LineAxis.Vertical);
            AddLine(750, 75, 750, 525, LineAxis.Vertical);
            AddLine(80, 140, 80, 280, LineAxis.Vertical);   
        }

        public void Run()
        {
            form.Paint += new PaintEventHandler(Draw);
            // form.Paint = form + new painteventhandler(draw) ? Eller form.Paint = new painteventhandler(draw, form.Paint) ?

            timer.Tick += new EventHandler(TimerEventHandler);
            timer.Interval = 1000 / 30;
            timer.Start();

            Application.Run(form);
        }

        private void AddBall()
        {
            var ball = new Ball(400, 300, 10);
            ball.Speed = new Vector(random.Next(10) - 5, random.Next(10) - 5);
            balls.Add(ball);
        }

        private void AddRectangle(int x, int y, int width, int height, RectangleState state)
        {
            var rectangle = new Rectangle(x, y, width, height, state);
            rectangles.Add(rectangle);
        }

        private void AddLine(int x1, int y1, int x2, int y2, LineAxis axis)
        {
            var line = new Line(new Point(x1, y1), new Point(x2, y2), axis);
            lines.Add(line);
        }

        private void TimerEventHandler(Object obj, EventArgs args)
        {
            if (random.Next(100) < 25) AddBall();

            foreach (var ball in balls)
            {
                ball.Move();

                foreach (var line in lines)
                {
                    line.Collision(ball);
                }
                foreach (var rectangle in rectangles)
                {
                    rectangle.Collision(ball);
                }
            }

            form.Refresh();
        }

        private void Draw(Object obj, PaintEventArgs args) // Skickar in args.Graphics som argument i ball.draw vilket tas emot som graphics g
        {
            foreach (var ball in balls)
            {
                ball.Draw(args.Graphics);
            }

            foreach (var rectangle in rectangles)
            {
                rectangle.Draw(args.Graphics);
            }

            foreach (var line in lines)
            {
                line.Draw(args.Graphics);
            }
        }
    }
}