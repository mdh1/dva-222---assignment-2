﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Assignment2
{
    public class Rectangle : Shape
    {
        private Engine.RectangleState State;
        private Pen LineColor;
        private int Width;
        private int Height;

        public Rectangle(int x, int y, int width, int height, Engine.RectangleState state) : base(new Point(x, y))
        {
            if (state == Engine.RectangleState.SpeedUp)
            {
                this.LineColor = new Pen(Color.Red);
            }
            else // SpeedDown
            {
                this.LineColor = new Pen(Color.Blue);
            }

            this.State = state;
            this.Width = width;
            this.Height = height;
        }

        override public void Draw(Graphics g)
        {
            g.DrawRectangle(this.LineColor, this.position.X, this.position.Y, this.Width, this.Height);
        }

        private void ReduceSpeed(Ball ball)
        {
            ball.Speed.X = ball.Speed.X * (float)0.6;
            ball.Speed.Y = ball.Speed.Y * (float)0.6;
        }

        private void ChangeSpeed(Ball ball, Engine.RectangleState state)
        {
            if (state == Engine.RectangleState.SpeedUp)
            {
                if (ball.Speed.X >= 0) { ball.Speed.X++; }
                else { ball.Speed.X--; }

                if (ball.Speed.Y >= 0) { ball.Speed.Y++; }
                else { ball.Speed.Y--; }
            }
            else // SpeedDown
            {
                if ((ball.Speed.X >= 1) && (ball.Speed.Y >= 1))
                {
                    ReduceSpeed(ball);
                }
                else if ((ball.Speed.X >= 1) && (ball.Speed.Y <= 1))
                {
                    ReduceSpeed(ball);
                }
                else if ((ball.Speed.X <= -1) && (ball.Speed.Y >= 1))
                {
                    ReduceSpeed(ball);
                }
                else if ((ball.Speed.X <= -1) && (ball.Speed.Y <= -1))
                {
                    ReduceSpeed(ball);
                }
                else if ((ball.Speed.X <= -1) && (ball.Speed.Y <= 1))
                {
                    ReduceSpeed(ball);
                }
                else if ((ball.Speed.X <= 1) && (ball.Speed.Y <= -1))
                {
                    ReduceSpeed(ball);
                }
            }
        }

        public void Collision(Ball ball)
        {
            if (ball.GetX() + ball.GetRadius() >= this.position.X &&
                ball.GetX() - ball.GetRadius() <= this.Width + this.position.X &&
                ball.GetY() + ball.GetRadius() >= this.position.Y &&
                ball.GetY() - ball.GetRadius() <= this.Height + this.position.Y)
            {
                ChangeSpeed(ball, State);
            }
        }
    }
}