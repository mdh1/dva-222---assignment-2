﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    class Line : Shape
    {
        private Engine.LineAxis Axis;
        private Pen LineColor;
        private Point StartPosition;
        private Point EndPosition;

        public Line(Point start, Point end, Engine.LineAxis axis) : base(start)
        {
            if (axis == Engine.LineAxis.Horizontal) 
            {
                LineColor = new Pen(Color.Green); 
            }
            else // Veritcal
            {
                LineColor = new Pen(Color.Yellow); 
            }

            this.Axis = axis;
            this.StartPosition = start;
            this.EndPosition = end;
        }

        public override void Draw(Graphics g)
        {
            g.DrawLine(LineColor, StartPosition, EndPosition);
        }

        public void Collision(Ball ball)
        {
            if (ball.GetX() + ball.GetRadius() >= this.StartPosition.X &&
                ball.GetX() - ball.GetRadius() <= this.EndPosition.X &&
                ball.GetY() + ball.GetRadius() >= this.StartPosition.Y &&
                ball.GetY() - ball.GetRadius() <= this.EndPosition.Y)
            {
                if (this.Axis == Engine.LineAxis.Horizontal)
                {
                    ball.Speed.Y = ball.Speed.Y * -1;    
                }
                else // Vertical
                {
                    ball.Speed.X = ball.Speed.X * -1;
                }
            }
        }
    }
}
